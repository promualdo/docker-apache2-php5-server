FROM phusion/baseimage

RUN apt-get install -f
RUN apt-get update -y
RUN apt-get dist-upgrade -y
RUN apt-get install apache2 -y
RUN apt-get install php5 -y
RUN apt-get install php5-mysqlnd -y
RUN apt-get clean -y

# Override the apache2 version that is ran by default
COPY apache.start /etc/service/apache2/run
COPY php.ini /etc/php5/apache2/php.ini
#
# /var/www is a volume mounted by docker
# At least in Mac OS the volumes mounted are prtected with umask 022
# Because of this only the file owner can cd into the dir and apache displays forbiden error message
#
# The mounted volume allways belongs to user with id=1000 that does not exist
# By creating a user with uid=1000 and using apache to run with that user we can work-arround this limitation
#
RUN useradd --uid 1000 --gid www-data apache2
#
# envvars is modified so that: export APACHE_RUN_USER=apache2 
# apache 2 is he new user created to avoid the volume permissions problem
#

EXPOSE 80
